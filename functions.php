<?php

/**
 * Print HTML with date information for current post.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param boolean $echo (optional) Whether to echo the date. Default true.
 * @return string The HTML-formatted post date.
 */
function twentythirteen_entry_date( $echo = true ) {
	// Non vogliamo la data sugli articoli.
	$date = '';

	if ( $echo )
		echo $date;

	return $date;
}
